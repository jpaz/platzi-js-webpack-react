import fetch from "node-fetch";

const API_URL = process.env.API_URL;
const API_MAX_ID = process.env.API_MAX_ID;

function getRandomId() {
	return Math.floor(Math.random() * API_MAX_ID);
}

function buildCharacter(apiResponse) {
	const email = apiResponse.name
		.replaceAll(/\ /g, "-")
		.toLowerCase()
		.concat("@example.com");
	return {
		...apiResponse,
		email,
	};
}

export default async function getRandomCharacter() {
	try {
		const urlAPI = `${API_URL}${getRandomId()}`;
		const response = await fetch(urlAPI);
		const apiResponse = await response.json();
		return buildCharacter(apiResponse);
	} catch (err) {
		console.error("Error on fetch character data" + error?.message);
	}
}
