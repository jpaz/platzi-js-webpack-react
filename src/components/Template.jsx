import github from "@assets/images/github.png";
import instagram from "@assets/images/instagram.png";
import twitter from "@assets/images/twitter.png";
import React from "react";

const Template = ({ character }) => (
	<div className="About">
		<div className="card">
			<div className="card_details">
				<div className="card_photo center circle">
					<img src={character.image} alt={character.name} />
					<svg
						viewBox="0 0 100 100"
						style={{
							enableBackgroud: "new -580 439 577.9 194",
						}}
					>
						<circle cx="50" cy="50" r="40" />
					</svg>
				</div>

				<p className="card_title">Hi, My name is</p>
				<p className="card_value">{character.name}</p>
			</div>

			<div className="card_userdata">
				<ul>
					<li>
						<a type="email" href={character.email}>
							{character.email}
						</a>
					</li>
					<li>{character.location.name}</li>
				</ul>
			</div>

			<div className="card_social">
				<a href="https://twitter.com/gndx">
					<img src={twitter} />
				</a>
				<a href="https://github.com/gndx">
					<img src={github} />
				</a>
				<a href="https://instagram.com/gndx">
					<img src={instagram} />
				</a>
			</div>
		</div>
	</div>
);

export default Template;
