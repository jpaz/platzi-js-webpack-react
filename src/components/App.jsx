import React, { useEffect, useState } from "react";
import "@styles/global.scss";
import Template from "@components/Template";
import getRandomCharacter from "@utils/getData";

const App = () => {
	const [character, setCharacter] = useState(null);

	const findCharacter = async () => {
		const character = await getRandomCharacter();
		setCharacter(character);
	};

	useEffect(() => {
		findCharacter();
	}, []);

	return character ? <Template character={character} /> : null;
};

export default App;
