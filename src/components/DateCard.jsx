import React from "react";
import { getCurrentLocalDate } from "@utils/dateUtils";

const DateCard = () => (
	<div>
		<h2>{getCurrentLocalDate()}</h2>
	</div>
);

export default DateCard;
